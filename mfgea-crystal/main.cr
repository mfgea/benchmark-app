require "http/server"
require "tempfile"

module BasicHttp
  class Base
    def initialize
      @routes = {} of String => ( -> String)
    end

    def run (port)
      server = HTTP::Server.new(port) do |context|
        if @routes.has_key?(context.request.path.to_s)
          context.response.content_type = "application/json"
          context.response.print @routes[context.request.path.to_s].call
        else
          context.response.status_code = 404
        end
      end
      puts "Listening on http://localhost:#{port}"
      server.listen
    end

    def get(route, &block : ( -> String))
      @routes[route.to_s] = block
    end
  end
end

def sha512 (data)
  raise "Invalid input" unless data
  hash = OpenSSL::Digest.new("SHA512")
  hash.update(data)
  hash.to_s
end

app = BasicHttp::Base.new

app.get "/" do
  "{\"status\":\"ok\", \"message\": \"Hello world!\", \"author\": \"Matias Gea <mgea@sparkdigital.com>\"}"
end

app.get "/throughput" do
  "{\"throughput\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in ipsum a velit faucibus tempor vel nec odio. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur quis orci eget purus tempus aliquet eu eu risus. Ut velit elit, viverra et ex vel, scelerisque rhoncus odio. Donec vitae diam pellentesque, commodo velit et, lacinia leo. In vel pharetra purus, sed eleifend nunc. Maecenas porta rhoncus consectetur. In hac habitasse platea dictumst. Duis sed erat nibh. Morbi imperdiet lorem purus, vitae facilisis enim maximus et. Phasellus ullamcorper sapien eget neque eleifend malesuada.\"}"
end

app.get "/cpu" do
  dataString = "Sparkers doing some benchmarking"
  hash = ""

  256.times do |counter|
    hash = sha512(dataString)
  end

  "{\"hashed\": \"#{hash}\"}"
end

app.get "/ram" do
  input_string = File.read("./ram_test.txt")
  vowels = input_string.gsub(/[^aeiouAEIOU]/, "").size

  "{\"n_vowels\": \"#{vowels}\"}"
end

app.get "/disk" do
  content = File.read("./disk_test.csv")
  tempfile = Tempfile.open("disk_test.temp.csv") do |file|
    file.print(content)
  end

  bytes = File.size(tempfile.path)
  "{\"bytes\": \"#{bytes}\"}"
end

ENV["PORT"] ||= "8080"
app.run ENV["PORT"].to_i